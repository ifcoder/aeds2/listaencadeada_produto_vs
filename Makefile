encadeada.exe: main.o Encadeada.o Nodo.o Produto.o
	g++ -o encadeada.exe main.o Encadeada.o Nodo.o Produto.o

main.o: main.cpp
	g++ -c main.cpp

Encadeada.o: Encadeada.cpp Encadeada.h
	g++ -c Encadeada.cpp

Nodo.o: Nodo.cpp Nodo.h
	g++ -c Nodo.cpp

Produto.o: Produto.cpp Produto.h
	g++ -c Produto.cpp

clean: 
	rm *.o